let loggedIn = localStorage.getItem("loggedIn");
let users = JSON.parse(localStorage.getItem("users"));

if (!loggedIn || !users || (users && !users.find((e) => e.name === loggedIn))) {
  window.location.href = "../views/index.html";
}
