// const todo = document.getElementById("todo");
// const description = document.getElementById("description");

const input = document.getElementById("input");
const btn = document.getElementById("btn_add");
const taskList = document.getElementById("list");

const setListeners = () => {
  const collection = document.getElementsByClassName("check-box");
  collection.forEach = [].forEach;
  collection.forEach((ch) => {
    ch.addEventListener("click", () => {
      console.log("click");
    });
  });
};

let todoList = [];
if (localStorage.getItem("todo")) {
  todoList = JSON.parse(localStorage.getItem("todo"));
  createDelElements();
}

btn.addEventListener("click", (e) => {
  let newTodo = {
    taskList: input.value,
    checked: false,
    important: false,
  };
  todoList.push(newTodo);
  createDelElements();
  localStorage.setItem("todo", JSON.stringify(todoList));
  input.value = "";
});

function createDelElements() {
  let createDelElement = "";
  todoList.forEach((item, i) => {
    createDelElement += `
        <div>
        <input class='check_box type='checkbox' id='item_${i}' ${
      item.checked ? "checked" : ""
    }>
        <label for='item_${i}'>${item.taskList}</label>
        <button id='item_del_${i}'>del</button>
        </div> `;
    taskList.innerHTML = createDelElement;
  });
}

taskList.addEventListener("change", (e) => {
  let idInput = e.target.getAttribute("id");
  let valueLabel = taskList.querySelector("[for=" + idInput + "]").innerHTML;
  todoList.forEach((item, i) => {
    if (item.taskList === valueLabel) {
      item.checked = !item.checked;
      localStorage.setItem("todo", JSON.stringify(todoList));
    }
  });
});

// const renderTasks = () =>
// list.map(({ name, description, id }, i) => (
//   <div key={id}>
//     <h5>{i + 1 + "." + name}</h5>
//     <input
//       className="input"
//       value={description}
//       onChange={(e) => onEditDescription(e, id)}
//     />
//     <button onClick={() => onDeleteTask(id)}>delete</button>
//     <hr />
//   </div>
// ));

// const onDeleteTask = (id) => {
//     const _list = [...list];
//     const index = _list.findIndex((task) => task.id === id);
//     _list.splice(index, 1);
//     setList(_list);
//   };

// taskList.addEventListener("click", (e) => {
//   todoList.forEach((item, i) => {
//     if () {

//       localStorage.setItem("todo", JSON.stringify(todoList));
//     }
//   });
// });
