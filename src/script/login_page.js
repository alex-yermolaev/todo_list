let users;

if (localStorage.getItem("users")) {
  users = JSON.parse(localStorage.getItem("users"));
} else {
  localStorage.setItem("users", JSON.stringify([]));
  users = [];
}

let submit = document.getElementById("submit");
let login = document.getElementById("login");
let password = document.getElementById("pass");

const onSubmit = () => {
  let _login = login.value;
  let _password = password.value;
  if (!_login || !_password) {
    alert("Please fill all field");
    return;
  }
  let user = users.find((el) => el.name === _login);
  if (user && user.password === _password) {
    localStorage.setItem("loggedIn", user.name);
    window.location.href = "../views/index.html";
  } else if (user && user.password !== _password) {
    alert("wrong password");
  } else {
    alert("User does not exist");
  }
};

submit.addEventListener("click", onSubmit);
